
public class Activitat2 {

	public static void main(String[] args) {
		//Declaro las variables
		int n = 6;
		double a = 2.3;
		char c = 'a';
		//Muestro por pantalla los valores
		System.out.println("Variable N:" + n);
		System.out.println("Variable A:" + a);
		System.out.println("Variable C:" + c);
		//Hago la suma i la resta de la variable int i double
		System.out.println(n + " + " + a + " = " + (n + a));
		System.out.println(a + " - " + n + " = " + (a - n));
		//Muetro el valor de char
		System.out.println("El valor del car�cter es: " + (int)c);

	}

}
